<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <img src="images/logo.png" alt="" class="header__logo">
        <div class="header__title_wrapper">
            <h1 class="header__title">
                Laboratory work №1 "Hello, world!"
            </h1>
        </div>
    </header>
    <main>
        <?php
        echo '<p class="main__content">Hello world!</p>';
        ?>
    </main>
    <footer>
        Создать веб-страницу с динамическим контентом и загрузить код в удалённый репозиторий, затем загрузить на хостинг. 
    </footer>
</body>
</html>